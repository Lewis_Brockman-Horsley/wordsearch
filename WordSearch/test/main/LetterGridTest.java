package main;

import static org.junit.Assert.*;

import org.junit.Test;

public class LetterGridTest {

	@Test
	public void testHorizontal() {
		// setup
		char[][] grid = {
				{'c','a','t'},
				{'t','a','c'},
				{'d','o','g'}
		};
		char[] searchWord = {'c','a','t'};
		int expected = 2;
		
		LetterGrid lg = new LetterGrid(grid);
		
		// execute
		int result = lg.searchWord(searchWord);
		
		// assert
		assertEquals(expected, result);
		
	}
	
	@Test
	public void testVertical() {
		// setup
		char[][] grid = {
				{'c','t','d'},
				{'a','a','o'},
				{'t','c','g'}
		};
		char[] searchWord = {'c','a','t'};
		int expected = 2;
		
		LetterGrid lg = new LetterGrid(grid);
		
		// execute
		int result = lg.searchWord(searchWord);
		
		// assert
		assertEquals(expected, result);
		
	}
	
	@Test
	public void testDiagonalDown() {
		// setup
		char[][] grid = {
				{'c','d','o','d'},
				{'t','a','d','o'},
				{'d','a','t','g'},
				{'d','o','c','g'},
		};
		char[] searchWord = {'c','a','t'};
		int expected = 2;
		
		LetterGrid lg = new LetterGrid(grid);
		
		// execute
		int result = lg.searchWord(searchWord);
		
		// assert
		assertEquals(expected, result);
		
	}
	
	@Test
	public void testDiagonalUp() {
		// setup
		char[][] grid = {
				{'o','d','g','d'},
				{'d','o','t','c'},
				{'d','a','a','g'},
				{'c','t','d','g'},
		};
		char[] searchWord = {'c','a','t'};
		int expected = 2;
		
		LetterGrid lg = new LetterGrid(grid);
		
		// execute
		int result = lg.searchWord(searchWord);
		
		// assert
		assertEquals(expected, result);
		
	}

}

package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LetterGrid {
	private String filename;
	private char[][] grid;
	private int rows;
	private int columns;

	public LetterGrid(String filename) {
		super();
		this.filename = filename;
		readGrid();
	}
	
	// Constructor for unit testing
	public LetterGrid(char[][] grid) {
		this.filename = "";
		this.grid = grid;
		this.rows = grid.length;
		this.columns = grid[0].length;
	}

	// Read Grid csv and store in char array
	private void readGrid() {

		// find out how many rows

		// count rows before grid initiliasation
		getGridSize();
		grid = new char[rows][columns];

		// find out how many columns
		// row.length

		String line = "";
		try {
			// parsing a CSV file into BufferedReader class constructor
			BufferedReader br = new BufferedReader(new FileReader(filename));

			// TODO BUG: The first three characters are not required.
			br.read();
			br.read();
			br.read();

			for (int i = 0; i < rows; i++) // returns a Boolean value
			{
				line = br.readLine();
				// parse csv data into char array
				String[] lineString = line.split(","); // remove the commas
				for (int j = 0; j < lineString.length; j++) {
					grid[i][j] = lineString[j].charAt(0); // convert strings to chars
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Find the size of the grid before initialisation.
	private void getGridSize() {
		// required: the length of each row is the same
		String line = "";
		String[] row = null;
		int rowCount = 0;
		try {
			// parsing a CSV file into BufferedReader class constructor
			BufferedReader br = new BufferedReader(new FileReader(filename));
			while ((line = br.readLine()) != null) // returns a Boolean value
			{
				row = line.split(",");
				rowCount++;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		rows = rowCount;
		columns = row.length;
	}

	public int searchWord(char[] word) {
		int findCount = 0; // counts how many times the word is found

		// Searches each direction for the word
		// must loop through array, words can begin anywhere
		// possible efficiency by looking at the length of the searched word
		// this will reduce the search area
		// the difference between the search functions is that the search index will change behavior
		// and the search area will be different
		findCount += searchHorizontal(word);
		findCount += searchVertical(word);
		findCount += searchDiagonalDown(word);
		findCount += searchDiagonalUp(word);

		return findCount;
	}

	// After getting one search mode working try to generalise it for the others
	private int searchHorizontal(char[] word) {

		int y = 0; // counts the letters and moves the search
		int findCount = 0;
		// loop through each starting point - unique index conditions
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns - word.length + 1; j++) { // the +1 ensures it looks to the edge

				// Search left to right
				if (grid[i][j] == word[0]) { // begin search if first letter is correct
					// search through each letter in the word
					for (char c : word) {
						if (j + y < columns) { // make sure the search index isn't out of range

							if (grid[i][j + y] == c) {
								y++;
							} else {
								y = 0;
							}

							// if we have found the complete word
							if (y == word.length) {
								findCount++;
								y = 0; // reset character counter
							}
						}
					}
				}
			}
		}
		y = 0;
		
		// Search Reverse - need separate loop because of different search area
		for (int i = 0; i < rows; i++) {
			for (int j = word.length - 1; j < columns; j++) { // the +1 ensures it looks to the edge

				// Search right to left
				if (grid[i][j] == word[0]) { // begin search if first letter is correct
					// search through each letter in the word
					for (char c : word) {
						if (j + y > 0) { // make sure the search index isn't out of range (y will be negative)

							if (grid[i][j + y] == c) {
								y--;
							} else {
								y = 0;
							}

							// if we have found the complete word
							if (y == word.length) {
								findCount++;
								y = 0; // reset character counter
							}
						}
					}
				}
			}
		}
		

		return findCount;
	}

	private int searchDiagonalUp(char[] word) {
		// TODO Auto-generated method stub
		return 0;
	}

	private int searchDiagonalDown(char[] word) {
		// TODO Auto-generated method stub
		return 0;
	}

	private int searchVertical(char[] word) {
		// TODO Auto-generated method stub
		return 0;
	}

}

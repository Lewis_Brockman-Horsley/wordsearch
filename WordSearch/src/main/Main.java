package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	

	public static void main(String[] args) throws IOException {
		
		// create grid object
		// grid is not a fixed dimension
		String filename = "grid.csv";
		LetterGrid grid = new LetterGrid(filename);
		
		// create read grid function
		
		// loop input from user to search grid
		BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
		String word = "";
		
		// Main Loop
		while (true) {
			System.out.print("Enter word to search (0 = end): ");
		     word = reader.readLine();
		     
		     // special case to end the program
		     if (word.equals("0")) {
		    	 return;
		     }
		     
		     System.out.println("Searching for: " + word);
		     
		     // search in 8 directions
		     int result = grid.searchWord(word.toCharArray());
		     
		     System.out.println(word + " was found " + result + " times.");
			
		}
	}

}
